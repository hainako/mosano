import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';

import React, { Component } from 'react';

import Form from './components/form';
import Table from './components/table';
import './App.css';
const APIUrl = 'https://restcountries.eu/rest/v2/all'
  
  
class App extends Component {
  constructor(props){
    super(props)
   this.state = {
     users: []
   }
   
  }
  componentWillMount(){
   let localUsers = localStorage.getItem('users')
   console.log(JSON.parse(localUsers))
   if (!Array.isArray(localUsers) || !localUsers.length) {
    
    } else {
      this.setState({
        users:  JSON.parse(localUsers)
      });
    }
  }
  componentDidMount(){
    let localUsers = localStorage.getItem('users')
   console.log(JSON.parse(localUsers))
   if(localUsers !== null){
    this.setState({
      users: JSON.parse(localUsers)
    });
   }
  }
  onFormSubmit = (user) =>{
     
    this.setState({
      users: [...this.state.users, user ]
    });
   
   
  }
  componentDidUpdate() {
   
    localStorage.setItem('users', JSON.stringify(this.state.users))
  }
  render() {
  
    return (
      
      <div className="App">

        <Form callbackFromParent={this.onFormSubmit}></Form>
        <Table usersData={this.state.users}></Table>
      </div>
    );
  }
}

export default App;
