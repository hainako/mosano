import React, { Component } from 'react';
import fetchJsonp from 'fetch-jsonp';
const APIUrl = 'https://restcountries.eu/rest/v2/all'

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {name: '',
        surname: '',
        birthday: '',
        country: '',
        countries: [],
        submitted: false}
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSurNameChange = this.handleSurNameChange.bind(this);
        this.handleBirthdayChange = this.handleBirthdayChange.bind(this);
        this.handleCountryChange = this.handleCountryChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        
    }  
    
   async componentDidMount(){
    const response = await fetch(APIUrl)
    const json = await response.json();
        this.setState({countries: json})
        this.setState({country: this.state.countries[0].name})
    }
    handleNameChange = (e) => {
        this.setState({name: e.target.value});
     }
     handleSurNameChange = (e) => {
        this.setState({surname: e.target.value});
     }
     handleBirthdayChange = (e) => {
        this.setState({birthday: e.target.value});
     }
     handleCountryChange = (e) => {
        this.setState({country: e.target.value});
     }
    onFormSubmit = (event) => {
        event.preventDefault()
        let user = {}
        user = {name: this.state.name, surname:this.state.surname,country: this.state.country, birthday:this.state.birthday}
        this.setState({submitted: true})
        this.props.callbackFromParent(user);
        /* let users = []
        users.push(user)
        localStorage.setItem('users', JSON.stringify(users)) */
   }
   renderUserInfo() {
    return  <div className="alert alert-info" role="alert">
    Hello {this.state.name} from {this.state.country}. On {this.state.birthday.split('-')[2]} of {this.state.birthday.split('-')[1]} you will have {(new Date()).getFullYear() - this.state.birthday.split('-')[0]}.
    </div>
     } 
    render() {
      return (
        <div className="col-md-6">   
        <form onSubmit={this.onFormSubmit}>
        <div className="form-group">
          <label htmlFor="exampleFormControlInput1">Name</label>
          <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="" value={this.state.name} onChange={this.handleNameChange}  />
        </div>
        <div className="form-group">
          <label htmlFor="exampleFormControlInput2">Surname</label>
          <input type="text" className="form-control" id="exampleFormControlInput2" placeholder="" value={this.state.surname} onChange={this.handleSurNameChange}  />
        </div>
        <div className="form-group">
          <label htmlFor="exampleFormControlSelect1">Countries</label>
          <select className="form-control" id="exampleFormControlSelect1" value={this.state.country} onChange={this.handleCountryChange}>
          {this.state.countries.map((item, i) => {
          return <option key={item.name} value={item.name}>{item.name}</option> })}
          </select>
        </div>
       
        <div className="form-group">
          <label htmlFor="exampleFormControlInput3">Birthday</label>
          <input type="date" className="form-control" id="exampleFormControlInput3" placeholder="" value={this.state.birthday} onChange={this.handleBirthdayChange}  />
        </div>

           <button className="btn btn-outline-info btn-lg btn-block" type="submit" > SAVE </button>
      </form>

        {this.state.submitted && this.renderUserInfo()}
      </div>
      );
    }
  }
  
  export default Form;