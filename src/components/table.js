import React, {Component} from 'react';


class Table extends Component {

    render(){
        
        return (
            <div className="col-md-6">
            <table className="table table-dark">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Country</th>
            <th scope="col">Birthday</th>
          </tr>
        </thead>
        <tbody>
        {this.props.usersData.map((item, i) => {
            return <tr key={i}>
            <th scope="row" >{i}</th>
            <td>{item.name}</td>
            <td>{item.country}</td>
            <td>{item.birthday}</td>
            </tr>
            })} 
        </tbody>
      </table>
      </div>
        )
    }
}

export default Table;